package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.SkuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku图片
 * 
 * @author liuhuaqi
 * @email liuhuaqi@gmail.com
 * @date 2022-12-01 22:06:36
 */
@Mapper
public interface SkuImagesDao extends BaseMapper<SkuImagesEntity> {
	
}
