package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.UmsMemberCollectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的商品
 * 
 * @author liuhuaqi
 * @email liuhuaqi@gmail.com
 * @date 2022-12-02 01:16:38
 */
@Mapper
public interface UmsMemberCollectSpuDao extends BaseMapper<UmsMemberCollectSpuEntity> {
	
}
