package com.atguigu.gulimall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 1.本服务想要远程调用其他服务
 *  1>本服务引入open-feign依赖
 *  2>本服务在其feign包内编写一个接口告诉springCLOud这个接口远程调用其他服务
 *  3>本服务启动类添加@EnableFeignClients开启远程调用其他服务的功能,全包名COPY reference
 */
@EnableFeignClients(basePackages ="com.atguigu.gulimall.member.feign")
@EnableDiscoveryClient
@SpringBootApplication
public class GulimallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallMemberApplication.class, args);
    }

}
