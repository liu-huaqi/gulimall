package com.atguigu.gulimall.member.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

//被调用者编写接口并引入注解@FeignClient注明所属服务名
@FeignClient(name ="gulimall-coupon",url ="http://localhost:7000")
public interface CouponFeignService {
//被调用者接口内的抽象方法需补全路径,即复制被调用方法的完整签名+补全完整路径
    @RequestMapping("/coupon/coupon/member/list")
    public R membercoupons();
}
