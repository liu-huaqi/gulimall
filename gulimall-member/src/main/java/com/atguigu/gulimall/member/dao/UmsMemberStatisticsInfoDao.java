package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.UmsMemberStatisticsInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员统计信息
 * 
 * @author liuhuaqi
 * @email liuhuaqi@gmail.com
 * @date 2022-12-02 01:16:38
 */
@Mapper
public interface UmsMemberStatisticsInfoDao extends BaseMapper<UmsMemberStatisticsInfoEntity> {
	
}
