package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.UmsMemberCollectSubjectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的专题活动
 * 
 * @author liuhuaqi
 * @email liuhuaqi@gmail.com
 * @date 2022-12-02 01:16:38
 */
@Mapper
public interface UmsMemberCollectSubjectDao extends BaseMapper<UmsMemberCollectSubjectEntity> {
	
}
