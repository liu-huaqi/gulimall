package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.UmsMemberReceiveAddressEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收货地址
 * 
 * @author liuhuaqi
 * @email liuhuaqi@gmail.com
 * @date 2022-12-02 01:16:38
 */
@Mapper
public interface UmsMemberReceiveAddressDao extends BaseMapper<UmsMemberReceiveAddressEntity> {
	
}
