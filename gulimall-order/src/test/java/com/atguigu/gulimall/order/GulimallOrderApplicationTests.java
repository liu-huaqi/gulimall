package com.atguigu.gulimall.order;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.atguigu.gulimall.order.entity.OrderReturnReasonEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Date;
import java.util.UUID;

@Slf4j
@SpringBootTest
class GulimallOrderApplicationTests {
    //@Autowired
    @Resource
    AmqpAdmin amqpAdmin;
    @Resource
    RabbitTemplate rabbitTemplate;
    @Test
    void sendMessageTest() {
        for (int i = 0; i < 10; i++) {

            if (i<=5){
                OrderReturnReasonEntity orderReturnReasonEntity = new OrderReturnReasonEntity();
                orderReturnReasonEntity.setId(1L);
                orderReturnReasonEntity.setCreateTime(new Date());
                orderReturnReasonEntity.setName("不想买了-"+i);
                String msg = "helloworld";
                rabbitTemplate.convertAndSend("hello-java-exchange",
                        "hello-java", orderReturnReasonEntity,new CorrelationData(UUID.randomUUID().toString()));
                log.info("消息发送完成{}", orderReturnReasonEntity);
            }else {
                OrderEntity orderEntity = new OrderEntity();
                orderEntity.setOrderSn(UUID.randomUUID().toString());
                rabbitTemplate.convertAndSend("hello-java-exchange",
                        "hello-java", orderEntity,new CorrelationData(UUID.randomUUID().toString()));
                log.info("消息发送完成{}", orderEntity);

            }

        }
    }
    @Test
    void creatEcxhange() {
        DirectExchange directExchange = new DirectExchange("hello-java-exchange",
                true,false);
        amqpAdmin.declareExchange(directExchange);
        log.info("Exchange[{}]创建成功","hello-java-exchange");
    }
    @Test
    void creatQueue() {
        Queue queue = new Queue("hello-java-queue", true, false, false);
//        Queue queue = new Queue("hello-java-queue", true, false, false);
        amqpAdmin.declareQueue(queue);
        log.info("Queue[{}]创建成功","hello-java-queue");
    }
    @Test
    void creatBinding() {
        Binding binding = new Binding("hello-java-queue",
                Binding.DestinationType.QUEUE,
                "hello-java-exchange",
                "hello-java", null);
        amqpAdmin.declareBinding(binding);
        log.info("Binding[{}]创建成功","hello-java-binding");
    }

    @Test
    void contextLoads() {
    }

}
