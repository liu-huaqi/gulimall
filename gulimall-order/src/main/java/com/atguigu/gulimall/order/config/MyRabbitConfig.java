package com.atguigu.gulimall.order.config;



import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;


/**
 * @author yaoxinjia
 * @email 894548575@qq.com
 */
@Configuration
public class MyRabbitConfig {

    private RabbitTemplate rabbitTemplate;

    @Primary
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        this.rabbitTemplate = rabbitTemplate;
        rabbitTemplate.setMessageConverter(messageConverter());
        initRabbitTemplate();
        return rabbitTemplate;
    }

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    /**
     * 定制RabbitTemplate
     * 1、服务收到消息就会回调
     *      1、spring.rabbitmq.publisher-confirms: true
     *      2、设置确认回调
     * 2、消息正确抵达队列就会进行回调
     *      1、spring.rabbitmq.publisher-returns: true
     *         spring.rabbitmq.template.mandatory: true
     *      2、设置确认回调ReturnCallback
     *
     * 3、消费端确认(保证每个消息都被正确消费，此时才可以broker删除这个消息)
     *
     */
    // @PostConstruct  //MyRabbitConfig对象创建完成以后，执行这个方法
    public void initRabbitTemplate() {

        /**
         * 1、只要消息抵达Broker就ack=true
         * correlationData：当前消息的唯一关联数据(这个是消息的唯一id)
         * ack：消息是否成功收到
         * cause：失败的原因
         */
        //设置确认回调
        rabbitTemplate.setConfirmCallback((correlationData,ack,cause) -> {
            System.out.println("confirm...correlationData["+correlationData+"]==>ack:["+ack+"]==>cause:["+cause+"]");
        });


        /**
         * 只要消息没有投递给指定的队列，就触发这个失败回调
         * message：投递失败的消息详细信息
         * replyCode：回复的状态码
         * replyText：回复的文本内容
         * exchange：当时这个消息发给哪个交换机
         * routingKey：当时这个消息用哪个路邮键
         */
        rabbitTemplate.setReturnsCallback(new RabbitTemplate.ReturnsCallback() {
            @Override
            public void returnedMessage(ReturnedMessage returnedMessage) {
                System.out.println("接收失败..fail message:"+returnedMessage.getMessage()+
                        "getReplyCode:"+returnedMessage.getReplyCode()+
                        "getReplyText:"+returnedMessage.getReplyText()+
                        "getExchange:"+returnedMessage.getExchange()+
                        "getRoutingKey:"+returnedMessage.getRoutingKey());

            }
        });}}




//@Configuration
//public class MyRabbitConfig {
/**
 * 原因：rabbitTemplate需要容器中已有messageConverter而MyRabbitmqConfig中又依赖了
 *
 * rabbitTemplate导致了循环依赖
 */
//    @Resource
//    RabbitTemplate rabbitTemplate;
//    @Bean
//    Jackson2JsonMessageConverter messageConverter(){
//        return new Jackson2JsonMessageConverter();
//
//    }
//
//    /**spring.rabbitmq.publisher-confirm-type=correlated
//     * 定制RabbitTemplate回调
//     * 消息到达服务器Broker-Exchange
//     * CorrelationData关联消息唯一ID
//     * ACK是否成功
//     * cause失败原因
//     *
//     */
//    @PostConstruct
//    void initRabbitTemplate(){
//        new RabbitTemplate.ConfirmCallback() {
//            @Override
//            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
//                System.out.println("confim...correlationData["+correlationData+
//                        "ack=>["+ack+"]cause=>["+cause+"]");
//
//            }
//
//
//    };
//
//        /**spring.rabbitmq.publisher-returns=true消息到队列
//         spring.rabbitmq.template.mandatory=true
//         */
//        rabbitTemplate.setReturnsCallback(new RabbitTemplate.ReturnsCallback() {
//            @Override
//            public void returnedMessage(ReturnedMessage returnedMessage) {
//                System.out.println("fail message:"+returnedMessage.getMessage()+
//                        "getReplyCode:"+returnedMessage.getReplyCode()+
//                        "getReplyText:"+returnedMessage.getReplyText()+
//                        "getExchange:"+returnedMessage.getExchange()+
//                        "getRoutingKey:"+returnedMessage.getRoutingKey());
//
//            }
//        });
//
//}}


