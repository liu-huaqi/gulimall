package com.atguigu.gulimall.order.controller;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.atguigu.gulimall.order.entity.OrderReturnReasonEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.UUID;
@Slf4j
@RestController
public class RabbitController {
    @Resource
    RabbitTemplate rabbitTemplate;
    @GetMapping("/sendmsg")
    String sendMessageTest(@RequestParam(value = "num",defaultValue = "10") Integer num) {
        for (int i = 0; i < num; i++) {

            if (i<5){
                OrderReturnReasonEntity orderReturnReasonEntity = new OrderReturnReasonEntity();
                orderReturnReasonEntity.setId(1L);
                orderReturnReasonEntity.setCreateTime(new Date());
                orderReturnReasonEntity.setName("不想买了-"+i);
                String msg = "helloworld";
                rabbitTemplate.convertAndSend("hello-java-exchange",
                        "hello-java", orderReturnReasonEntity,new CorrelationData(UUID.randomUUID().toString()));
                log.info("消息发送完成{}", orderReturnReasonEntity);
            }else {
                OrderEntity orderEntity = new OrderEntity();
                orderEntity.setOrderSn(UUID.randomUUID().toString());
                rabbitTemplate.convertAndSend("hello-java-exchange",
                        "hello22-java", orderEntity,new CorrelationData(UUID.randomUUID().toString()));
                log.info("消息发送完成{}", orderEntity);

            }

        }
        return "ok";
    }

}
